package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const VERSION = "v0.1.0"
const HEADER = "# pxname,svname,qcur,qmax,scur,smax,slim,stot,bin,bout,dreq," +
	"dresp,ereq,econ,eresp,wretr,wredis,status,weight,act,bck,chkfail,chkdown," +
	"lastchg,downtime,qlimit,pid,iid,sid,throttle,lbtot,tracked,type,rate," +
	"rate_lim,rate_max,check_status,check_code,check_duration,hrsp_1xx," +
	"hrsp_2xx,hrsp_3xx,hrsp_4xx,hrsp_5xx,hrsp_other,hanafail,req_rate," +
	"req_rate_max,req_tot,cli_abrt,srv_abrt,comp_in,comp_out,comp_byp,comp_rsp," +
	"lastsess,last_chk,last_agt,qtime,ctime,rtime,ttime,agent_status," +
	"agent_code,agent_duration,check_desc,agent_desc,check_rise,check_fall," +
	"check_health,agent_rise,agent_fall,agent_health,addr,cookie,mode,algo," +
	"conn_rate,conn_rate_max,conn_tot,intercepted,dcon,dses,\nhaproxy,BACKEND," +
	"0,0,0,0,0,0,0,0,0,0,,0,0,0,0,DOWN,1,1,0,,0,0,0,,1,0,0,,0,,1,0,,0,,,,0,0,0," +
	"0,0,0,,,,0,0,0,0,0,0,0,0,,,0,0,0,0,,,,,,,,,,,,,,\n"
const FRONTEND = "%s,FRONTEND,,,0,0,0,0,0,0,0,0,0,,,,,OPEN,,,,,,,,,1,0,0,,,," +
	"0,0,0,0,,,,0,0,0,0,0,0,,0,0,0,,,0,0,0,0,,,,,,,,,,,,,,,,,,,,,http,,0,0,0,0," +
	"0,0,\n"
const BACKEND = "%[1]s,%[1]s,0,0,0,0,,0,0,0,,0,,0,0,0,0,no check,1,1,0,,,0,,," +
	"1,0,1,,0,,2,0,,0,,,,0,0,0,0,0,0,,,,,0,0,,,,,0,,,0,0,0,0,,,,,,,,,,,,,,http," +
	",,,,,,,\n%[1]s,BACKEND,0,0,0,0,0,0,0,0,0,0,,0,0,0,0,DOWN,1,1,0,,0,0,0,,1," +
	"0,0,,0,,1,0,,0,,,,0,0,0,0,0,0,,,,0,0,0,0,0,0,0,0,,,0,0,0,0,,,,,,,,,,,,,," +
	"http,,,,,,,,\n"

type frontendArray []string

func (i *frontendArray) String() string {
    return ""
}

func (i *frontendArray) Set(value string) error {
    *i = append(*i, fmt.Sprintf(FRONTEND, value))
    return nil
}

type backendArray []string

func (i *backendArray) String() string {
    return ""
}

func (i *backendArray) Set(value string) error {
    *i = append(*i, fmt.Sprintf(BACKEND, value))
    return nil
}

var frontends frontendArray
var backends backendArray
var data string
var https bool
var insecure bool

func useHttps() string {
	if https {
		return "https"
	} else {
		return "http"
	}
}

func stats(w http.ResponseWriter, r *http.Request) {
	url := fmt.Sprintf("%s://172.17.0.1/%s", useHttps(), r.URL.Path[1:])
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		fmt.Fprintf(w, HEADER + data)
		return
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{ InsecureSkipVerify: insecure },
	}

	client := &http.Client{ Transport: tr }
	req.Host = "haproxy"
	resp, err := client.Do(req)

	if err != nil {
		fmt.Fprintf(w, HEADER + data)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Fprintf(w, HEADER + data)
		return
	}

	fmt.Fprintf(w, string(body))
}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "HAStats %s\n", VERSION)
	fmt.Fprintf(w, "http://127.0.0.1/;csv\n")
	fmt.Fprintf(w, "http://127.0.0.1/;csv;norefresh\n")
}

func main() {
	flag.Var(&frontends, "f", "an HAProxy frontend")
	flag.Var(&backends, "b", "an HAProxy backend")
	flag.BoolVar(&https, "https", false, "use HTTPS when connecting")
	flag.BoolVar(&insecure, "insecure", false, "skip validating SSL certificate")
	flag.Parse()

	data += strings.Join(frontends[:], "")
	data += strings.Join(backends[:], "")

	http.HandleFunc("/", index)
	http.HandleFunc("/;csv", stats)
	http.HandleFunc("/;csv;norefresh", stats)
	http.ListenAndServe(":80", nil)
}
